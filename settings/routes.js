'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


    // .......................users routes..............................
    app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);
    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);

    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);

    app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
    app.post('/api/users/resetPassword', auth.context.builder, validator.users.resetPassword, api.users.resetPassword);
    app.post('/api/users/changePassword', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);

    app.post('/api/users/logOut', auth.context.builder, auth.context.requiresToken, api.users.logOut)

    // ................................upload files............................................
    app.post('/api/files', auth.context.builder, multipartMiddleware, api.files.create)
    app.post('/api/files/upload', multipartMiddleware, api.files.upload);
    app.get('/api/files/:id', auth.context.builder, api.files.getById)
    // app.get('/api/files', auth.context.builder, api.files.get);

    // ................................product routes............................................
    app.post('/api/products', auth.context.builder, auth.context.requiresToken, validator.products.canCreate, api.products.create);
    app.get('/api/products/:id', auth.context.builder, auth.context.requiresToken, validator.products.getById, api.products.getById)
    app.get('/api/products', auth.context.builder, auth.context.requiresToken, api.products.get)
    app.put('/api/products/:id', auth.context.builder, auth.context.requiresToken,validator.products.update, api.products.update)

    // ...................................trade-enquiries routes...................................
    app.post('/api/tradeEnquiries', auth.context.builder, auth.context.requiresToken, validator.tradeEnquiries.canCreate, api.tradeEnquiries.create);
    app.get('/api/tradeEnquiries/:id', auth.context.builder, auth.context.requiresToken, validator.tradeEnquiries.getById, api.tradeEnquiries.getById)
    app.get('/api/tradeEnquiries', auth.context.builder, auth.context.requiresToken, api.tradeEnquiries.get)

    log.end();
}
exports.configure = configure