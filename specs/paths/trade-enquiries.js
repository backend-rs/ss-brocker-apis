module.exports = [{
    url: '/',
    post: {
        summary: 'Create',
        description: 'Create enquiry',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of enquiry creation',
            required: true,
            schema: {
                $ref: '#/definitions/tradeEnquiryCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    get: {
        summary: 'Get',
        description: 'get all trade enquiries',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        },{
            in: 'query',
            name: 'type',
            description: 'today/lastMonth/particularDate',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'fromDate',
            description: 'get enquiry for particular date',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'toDate',
            description: 'get enquiry for particular date',
            required: false,
            type: 'string'
        },,{
            in: 'query',
            name: 'pageNo',
            description: 'pageNo',
            required: false,
            type: 'string'
        },{
            in: 'query',
            name: 'items',
            description: 'items',
            required: false,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }

    }
}, {
    url: '/{id}',
    get: {
        summary: 'Get',
        description: 'get enquiry by Id',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'path',
            name: 'id',
            description: 'enquiryId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
}]