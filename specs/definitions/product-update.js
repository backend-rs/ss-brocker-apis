module.exports = {
    name: 'string',
    status:'string',
    image: {
        url: 'string',
        resize_url: 'string',
        thumbnail: 'string',
        resize_thumbnail: 'string'
    }
}