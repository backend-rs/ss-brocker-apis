
module.exports = {
    userId:'string',
    phone:'string',
    email:'string',
    address:'string',
    products:[{
        productId: 'string',
        quantity:'string',
        query:'string'
    }]
}