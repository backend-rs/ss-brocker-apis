module.exports = {
    firstName: 'string',
    lastName: 'string',
    email: 'string',
    address: {
        line1: 'string',
        line2: 'string'
    },
    image: {
        url: 'string',
        thumbnail: 'string',
        resize_url: 'string',
        resize_thumbnail: 'string'
    }
}