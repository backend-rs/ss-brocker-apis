'use strict'
const message = require('../helpers/message');
const date = require('../helpers/date');
const moment = require('moment');

const create = async (req, model, context, res) => {
    const log = context.logger.start('services/tradeEnquiries')
    try {
        // let now = await date.date();

        let user = await db.user.findById(model.userId);
        if (!user) {
            throw new Error(message.userError);
        }

        const enquiry = await new db.tradeEnquiry(model).save();
        // enquiry.date = now

        log.end();
        return enquiry
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/tradeEnquiries/getById:${id}`)
    try {
        const enquiry = await db.tradeEnquiry.findById(id);
        log.end();
        return enquiry
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, page, context) => {
    const log = context.logger.start(`services/tradeEnquiries/get`)
    try {
        let now = moment();
        let params = req.query
        let start
        let end
        let enquiries
        let user
        let product
        let query = {}
        if (params && params.type) {
            if (params.type == 'today') {
                start = now.clone().startOf('day');
                end = now.clone().endOf('day');
            }
            if (params.type == 'lastMonth') {
                var month = moment().subtract(1, 'month');
                start = month.clone().startOf('month');
                end = month.clone().endOf('month');
            }
            if (params.type == 'particularDate') {
                if (!params.fromDate) {
                    throw new Error('fromDate is required')
                }
                if (!params.toDate) {
                    throw new Error('toDate is required')
                }
                let fromDate = moment(params.fromDate, 'DD-MM-YYYY');
                let toDate = moment(params.toDate, 'DD-MM-YYYY');
                start = fromDate.clone().startOf('day');
                end = toDate.clone().endOf('day');
            }
            query = {
                timeStamp: {
                    $gte: start,
                    $lte: end
                }
            }
        } else {
            query = {}
        }
        if (page != undefined && page != null && page != "") {
            enquiries = await db.tradeEnquiry.find(query).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            });
        } else {
            enquiries = await db.tradeEnquiry.find(query);
        }
        if (enquiries && enquiries.length != 0) {
            for (let item of enquiries) {
                if (item) {
                    if (item.userId) {
                        user = await db.user.findById(item.userId);
                        item.userName = user.userName;
                    }
                    if (item.products && item.products.length != 0) {
                        for (let productItem of item.products) {
                            if (productItem) {
                                product = await db.product.findById(productItem.productId)
                            }
                            productItem.productName = product.name;
                        }
                    }
                }
            }
        }
        log.end();
        return enquiries

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get