'use strict'

const set = (model, entity, context) => {
    const log = context.logger.start('services/produ/set')

    if (model.name) {
        entity.name = model.name
    }
    if (model.status) {
        entity.status = model.status
    }
    if (model.image) {
        if (model.image.url) {
            entity.image.url = model.image.url
        }
        if (model.image.thumbnail) {
            entity.image.thumbnail = model.image.thumbnail
        }
        if (model.image.resize_url) {
            entity.image.resize_url = model.image.resize_url
        }
        if (model.image.resize_thumbnail) {
            entity.image.resize_thumbnail = model.image.resize_thumbnail
        }
    }

    log.end()
    return entity.save()
}
const create = async (req, model, context, res) => {
    const log = context.logger.start('services/products')
    try {
        const product = await new db.product(model).save();
        log.end();
        return product
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const getById = async (id, context) => {
    const log = context.logger.start(`services/products/getById:${id}`)
    try {
        const product = await db.product.findById(id);
        log.end();
        return product
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const get = async (req, page, context) => {
    const log = context.logger.start(`services/products/get`)
    try {
        let params = req.query
        let query = {}
        let product
        if (params) {
            if (params.status) {
                query = {
                    status: params.status
                }
            }
            if (params.name) {
                query = {
                    name: { $regex: params.name.toLowerCase() }
                }
            }
            if (params.status && params.name) {
                query = {
                    name: { $regex: params.name.toLowerCase() },
                    status: params.status
                }
            }
        } else {
            query = {}
        }

        // if paging
        if (page != null && page != undefined && page != "") {
            product = await db.product.find(query).skip(page.skipCount).limit(page.items).sort({
                timeStamp: -1
            })
        } else {
            product = await db.product.find(query)
        }

        log.end();
        return product
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
const update = async (id, model, context) => {
    const log = context.logger.start(`services/products:${id}`)
    try {
        const entity = await db.product.findById(id)

        if (!entity) {
            throw new Error('invalid user')
        }

        // call set method to update user
        await set(model, entity, context);
        return entity

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
