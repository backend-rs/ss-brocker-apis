'use strict'
const encrypt = require('../permit/crypto')
const auth = require('../permit/auth')
const randomize = require('randomatic');
const nodemailer = require('nodemailer');
const response = require('../exchange/response');
const message = require('../helpers/message');
const helpers = require('../helpers/mail-html');
const otp = require('../helpers/otp');

const set = (model, entity, context) => {
    const log = context.logger.start('services/users/set')

    if (model.firstName) {
        entity.firstName = model.firstName
    }
    if (model.lastName) {
        entity.lastName = model.lastName
    }
    if (model.email) {
        entity.email = model.email
    }
    if (model.address) {
        if (model.address.line1) {
            entity.address.line1 = model.address.line1
        }
        if (model.address.line2) {
            entity.address.line2 = model.address.line2
        }
    }
    if (model.image) {
        if (model.image.url) {
            entity.image.url = model.image.url
        }
        if (model.image.thumbnail) {
            entity.image.thumbnail = model.image.thumbnail
        }
        if (model.image.resize_url) {
            entity.image.resize_url = model.image.resize_url
        }
        if (model.image.resize_thumbnail) {
            entity.image.resize_thumbnail = model.image.resize_thumbnail
        }
    }
    log.end()
    return entity
}

const create = async (req, model, context, res) => {
    const log = context.logger.start('services/users')
    try {
        let user;
        let password;
        let userName;

        //create & encrypt password
        // password = randomize('0', 6);
        password = '123456';
        model.password = encrypt.getHash(password, context);

        // create userName
        userName = model.firstName + '_' + model.lastName + randomize('0', 3);
        model.userName = userName;

        if (model.phone) {
            //find user 
            user = await db.user.findOne({
                'phone': {
                    $eq: model.phone
                }
            })
            if (!user) {
                // create user
                user = await new db.user(model).save()

                // transporter
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                        user: 'ssbrokers6@gmail.com',
                        pass: 'brokers@123'
                    }
                });

                // mail details
                const subject = "New user registration"
                const text = "New user registration"
                //  user detail mail format
                const userDetails = helpers.mailFormat(user, password);

                // call sendMail method
                await otp.sendMail('ssbrokers6@gmail.com', transporter, subject, text, userDetails)
                res.message = message.userRegistrationMessage
                log.end()
                return response.data(res, '')

            } else if (user && user.status == 'pending') {
                res.message = message.alreadySubmittReqMessage
                log.end()
                return response.pendingReq(res, '');

            } else {
                log.end()
                throw new Error(message.userExists)
            }
        }
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/users/getById:${id}`)

    try {
        const user = id === 'my' ? context.user : await db.user.findById(id)
        log.end()
        return user

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/users:${id}`)
    try {

        const entity = id === 'my' ? context.user : await db.user.findById(id)

        if (!entity) {
            throw new Error(message.userError);
        }

        // call set method to update user
        await set(model, entity, context)
        log.end()
        return entity.save()
    } catch (err) {
        throw new Error(err)
    }
}

const login = async (model, context) => {
    const log = context.logger.start(`services/users/login`)

    try {
        let user;
        const query = {}

        if (model.userName) {
            query.userName = model.userName
        }

        // find user
        user = await db.user.findOne(query)

        if (!user) { // user not found
            log.end()
            throw new Error(message.userError)

        } else {

            // match password
            const isMatched = encrypt.compareHash(model.password, user.password, context)
            if (!isMatched) {
                log.end()
                throw new Error(message.passwordError)
            }

            // create token
            const token = auth.getToken(user._id, false, context)
            if (!token) {
                throw new Error("token error")
            }
            user.token = token;
            user.status = 'approved';
            user.save();
        }
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const changePassword = async (model, context) => {
    const log = context.logger.start(`services/users/changePassword`)

    try {
        // find user
        const entity = await db.user.findOne({
            '_id': {
                $eq: context.user.id
            }
        })
        if (!entity) {
            throw new Error(message.userError)
        }

        // match old password
        const isMatched = encrypt.compareHash(model.password, entity.password, context)
        if (!isMatched) {
            throw new Error(message.compareOldPassword);
        }

        // update & encrypt password
        entity.password = encrypt.getHash(model.newPassword, context)

        log.end()
        return entity.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const forgotPassword = async (model, context) => {
    const log = context.logger.start(`services/users/forgotPassword`)

    try {
        let user
        if (model.email) {
            // find user
            user = await db.user.findOne({
                'email':  model.email
            })
        }
        if (!user) {
            throw new Error(message.userError)
        }
        // call send otp function
        await otp.sendOtp(model, user, context)
        user.save()

        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const resetPassword = async (model, context) => {
    const log = context.logger.start(`services/users/verifyOtp`)

    try {
        let user;
        // find user
        user = await db.user.findOne({
            'email':  model.email
        })
        if (!user) {
            throw new Error(message.userError)
        }

        // call matchOtp method
        await otp.matchOtp(model, user, context);

        // update password
        if (user) {
            user.password = encrypt.getHash(model.newPassword, context)
            user.save()
        }
        log.end()
        return user;

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const logOut = async (res, context) => {
    const log = context.logger.start(`services/users/logOut`)

    try {
        const user = await db.user.findOne({
            '_id': {
                $eq: context.user.id
            }
        })
        if (!user) {
            throw new Error(message.userError)
        }
        user.token = ''
        user.save()
        res.message = message.logOutMessage
        log.end()
        return response.data(res, '')
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.update = update
exports.login = login
exports.forgotPassword = forgotPassword
exports.resetPassword = resetPassword
exports.changePassword = changePassword
exports.logOut = logOut


























// // send mail method
// const sendMail = async (email, transporter, subject, text, html) => {
//     const details = {
//         from: 'ssbrokers6@gmail.com',
//         to: email,
//         subject: subject,
//         text: text,
//         html: html
//     };
//     var info = await transporter.sendMail(details);
//     console.log("INFO:::", info)
// }

// // sendOtp method
// const sendOtp = async (model, user, context) => {

//     // transporter
//     var transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//             user: 'ssbrokers6@gmail.com',
//             pass: 'brokers@123'
//         }
//     });

//     // generate otp
//     const otp = randomize('0', 4)
//     user.otp = otp


//     const subject = "Your one time otp for ssBrockers is: "
//     const text = "The verification code for ssBrockers is:"

//     // call sendMail method
//     // await sendMail(model.email, transporter, subject, text, otp)

//     // generate expiryTime
//     const date = new Date();
//     const expiryTime = moment(date.setMinutes(date.getMinutes() + 30));
//     user.expiryTime = expiryTime
// }

// // match otp
// const matchOtp = async (model, user, context) => {

//     // match otp expiry time
//     const a = moment(new Date()).format();
//     const mom = moment(user.expiryTime).subtract(60, 'minutes').format();
//     const isBetween = moment(a).isBetween(mom, user.expiryTime)
//     if (!isBetween) {
//         throw new Error('Invalid otp or otp expired')
//     }

//     // match otp
//     if (model.otp === user.otp || model.otp == '5554') {

//     } else {
//         throw new Error("Otp did not match")
//     }

//     user.otp = ''
//     user.expiryTime = ''

// }
