'use strict'

// users create mapper
exports.toModel = entity => {

    var model = {
        _id: entity._id,
        type: entity.type,
        firstName: entity.firstName,
        lastName: entity.lastName,
        firmName: entity.firmName,
        email: entity.email,
        phone: entity.phone
    }
    if (entity.address) {
        model.address = {
            line1: entity.address.line1,
            line2: entity.address.line2
        }
    }
    if (entity.image) {
        model.image = {
            url: entity.image.url,
            thumbnail: entity.image.thumbnail,
            resize_url: entity.image.resize_url,
            resize_thumbnail: entity.image.resize_thumbnail
        }
    }
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        firstName: entity.firstName,
        lastName: entity.lastName,
        firmName: entity.firmName,
        email: entity.email,
        phone: entity.phone
    }
    if (entity.address) {
        model.address = {
            line1: entity.address.line1,
            line2: entity.address.line2
        }
    }
    if (entity.image) {
        model.image = {
            url: entity.image.url,
            thumbnail: entity.image.thumbnail,
            resize_url: entity.image.resize_url,
            resize_thumbnail: entity.image.resize_thumbnail
        }
    }
    return model
}

// for login 
exports.toUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        status:entity.status,
        isVerified: entity.isVerified,
        userName: entity.userName,
        firstName: entity.firstName,
        lastName: entity.lastName,
        firmName: entity.firmName,
        email: entity.email,
        token: entity.token,
        phone: entity.phone,
    }
    return model
}