'use strict'
const response = require('../exchange/response');
const validator = require('validator');


exports.canCreate = (req, res, next) => {
    if (!req.body.userId) {
        response.failure(res, 'UserId  is required');
    }
    if (!req.body.phone) {
        response.failure(res, 'Phone is required');
    }
    // if (req.body.phone && !validator.isMobilePhone(req.body.phone)) {
    //     response.failure(res, 'Phone number should be valid');
    // }
    if (!req.body.email) {
        response.failure(res, 'Email is required');
    }
    // if (req.body.email && !validator.isEmail(req.body.email)) {
    //     response.failure(res, 'Email should be valid');
    // }
    if (!req.body.address) {
        response.failure(res, 'Address is required');
    }
    if (!req.body.products) {
        response.failure(res, 'Products are required');
    }
    return next()
}
exports.getById = (req, res, next) => {
    if (!req.params.id) {
        response.failure(res, 'id is required');
    }
    return next()
}

exports.update = (req, res, next) => {
    if (!req.params.id) {
        response.failure(res, 'id is required');
    }
    return next()
}