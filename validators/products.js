'use strict'
const response = require('../exchange/response');

exports.canCreate = (req, res, next) => {
    if (!req.body.name) {
        response.failure(res, 'Product name is required');
    }
    if (!req.body.image) {
        response.failure(res, 'Product image is required');
    }
    return next()
}
exports.getById = (req, res, next) => {
    if (!req.params.id) {
        response.failure(res, 'id is required');
    }
    return next()
}

exports.update = (req, res, next) => {
    if (!req.params.id) {
        response.failure(res, 'id is required');
    }
    return next()
}