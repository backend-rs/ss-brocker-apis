'use strict'

module.exports = {
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    image: {
        url: String,
        resize_url: String,
        thumbnail: String,
        resize_thumbnail: String
    },
    status:{
        type:String,
        default:'active',
        enum:['active','inactive']
    }
}