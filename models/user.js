'use strict'

// User Module
module.exports = {
    type: {
        type: String,
        default: 'user',
        enum: ['user', 'admin']
    },
    status:{
        type:String,
        default:'pending',
        enum:['pending','approved']
    },
    userName: {
        type: String,
        lowercase: true,
        trim: true
    },
    firstName: {
        type: String,
        lowercase: true,
        trim: true
    },
    lastName: {
        type: String,
        lowercase: true,
        trim: true
    },
    firmName: {
        type: String,
        lowercase: true,
        trim: true
    },
    email: {
        type: String,
        lowercase: true,
        trim: true
    },
    image: {
        url: String,
        thumbnail: String,
        resize_url: String,
        resize_thumbnail: String
    },
    address: {
        line1: String,
        line2: String
    },
    phone: {
        type: String,
        trim: true
    },
    password: String,
    token: String,
    expiryTime: String,
    otp: String
}