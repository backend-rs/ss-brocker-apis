'use strict'

module.exports = {
    userId: String,
    userName: String,
    phone: String,
    email: String,
    address: String,
    products: [{
        productId: String,
        productName: String,
        quantity: String,
        query: String
    }]
}